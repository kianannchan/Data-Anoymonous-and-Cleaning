import PySimpleGUI as sg
import os


class interfaceClass:
    # constructor declare elements
    def __init__ (self):
        self.browse_input = sg.InputText('', size=(70,1), readonly= True, enable_events=True, key='browse_file')
        self.browse_button = sg.FileBrowse('Browse', size=(19,1), key='browse_button', initial_folder=os.getcwd(), target='browse_file', file_types=(("Excel File", "*.xlsx;*.xls;*.csv"),))
        self.encap_list_default = sg.Listbox(values=(), size=(36, 10), key = 'encap_list_default')
        self.encap_add = sg.Button('<<', size=(10,2), key='remove_en', pad=(0,0))
        self.encap_remove = sg.Button('>>', size=(10,2), key='add_en', pad=(0,30))
        self.encap_list_add = sg.Listbox(values=(), size=(36, 10), key ='encap_list_add')
        self.remove_list_default = sg.Listbox(values=(), size=(36, 10), key = 'remove_list_default')
        self.remove_add = sg.Submit('<<', size=(10,2), key='remove_remove', pad=(0,0))
        self.remove_remove = sg.Submit('>>', size=(10,2), key='remove_add', pad=(0,30))
        self.remove_list_add = sg.Listbox(values=(), size=(36, 10), key= 'remove_list_add')
        self.progressBar = sg.ProgressBar(100, orientation='h', size=(60, 20), key='progressbar')
        self.passwordLabel = sg.Text('Password')
        self.password_Input = sg.InputText('', key='password', password_char='*', size=(60,1), disabled=True)
        self.process_button = sg.Button('Process', size=(19,1), key='process', disabled=True)
        self.dropdown_label = sg.Text('Merge Data Option')
        self.text_area = sg.Multiline(size=(90,5), key='-ML-', autoscroll=True, disabled= True)


    # layout declaration
    def layout(self):
        encapColumn = [[self.encap_remove], [self.encap_add]]
        removeColumn = [[self.remove_remove], [self.remove_add]]
        return [
                [self.browse_input, self.browse_button],
                [sg.Frame('Encapsulation', 
                    [
                        [self.encap_list_default, sg.Column(encapColumn) , self.encap_list_add]
                    ])],
                [sg.Frame('Remove', 
                    [
                        [self.remove_list_default, sg.Column(removeColumn) , self.remove_list_add],
                    ])],
                [self.progressBar],
                [self.passwordLabel, self.password_Input, self.process_button],
                [sg.Frame('Logging', 
                    [
                    [self.text_area]
                    ])]
            ]
    
    # load element on window
    def load(self):
        return sg.Window('Data Anoymonous And Cleaning (DAAC) V0.0.0').Layout(self.layout())
    
    # popup message
    def popup(self, message):
        sg.popup_ok(message)
    
    def popup_yesno(self, message):
        return sg.popup_yes_no(message)  # Shows Yes and No buttons

    # tray message
    def tray(self, message):
        sg.SystemTray.notify('Notification', message )
    
     # save file dialog
    def saveFile(self, filename):
        return sg.popup_get_file('', no_window=True, save_as=True, initial_folder=os.getcwd(), default_path=filename, file_types=(("Excel File", "*.xlsx"),))


