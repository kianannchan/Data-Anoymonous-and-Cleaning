
import Cipher
from pathlib import Path
import openpyxl
import xlsxwriter
import json
import pandas as pd
import numpy as np
from playsound import playsound
import math
from time import time, ctime



class Settings:
    def __init__(self):
        self.defaultEncapsulationList = ['category']
        self.defaultRemovedList = ['postal']
        self.defaultPassword = "doraemon"
        self.cipherObj = Cipher.cipherClass()
        self.setPassword(self.defaultPassword)
        self.log = ''

    def setPassword(self, password):
        """
        set passphrase\\
        param: password: Str\\
        output: None
        """  
        self.defaultPassword = password
        self.cipherObj.setKey(password)
        
    def getTimeEpochTime(self):
        """
        get curr system time and return\\
        param: None\\
        output: time : datetime in millisecond
        """  
        return time()
    
    def getDateTimeReadable(self):
        return ctime()
    
    
    def getTimeDiff(self, prev):
        """
        get time diff\\
        param: prev: datetime\\
        output: time : datetime in millisecond
        """  
        return time() - prev
            

class Model(Settings):
        
    def __init__(self):
        Settings.__init__(self)
        self.encapsulationList = []
        self.removeList =[]
        self.df = pd.DataFrame() 
        self.filePath = ''
        self.mode = 1
        self.createFile = False


    def setFilePath(self,filePath):
        """
        Read and Set file path to class variable\\
        param: fname: String\\
        output: None
        """
        self.filePath = filePath
        if 'xls' in filePath:
            self.df = pd.read_excel(filePath, sheet_name=None, ignore_index=True)
        else:
            self.df = pd.read_csv(filePath)
            

    def detectCommonHeader(self):
        """
        Determine source file header exist\\
        param: None\\
        output: Bool
        """
        for sheet in range(len(self.df)):
            col= self.df[list(self.df.keys())[sheet]].columns.tolist()
            if sheet != len(self.df) - 1:
                col2= self.df[list(self.df.keys())[sheet + 1]].columns.tolist()
                if (col != col2):
                    return False
        return True
    

    def SubsequentHeader(self):
        """
        Parse sheets data into singular sheet base on common header\\
        param: None\\
        output: None
        """  
        self.df = pd.concat(self.df)
        self.df.replace(np.nan, '' , inplace=True)
    

    def noSubsequentHeader(self):
        """
        Parse sheets data into singular sheet base on casading mode\\
        param: None\\
        output: None
        """    
        col= self.df[list(self.df.keys())[0]].columns.tolist()
        list_DF =[]

        # accessing from data
        for index in range(len(self.df)):
            subsequent_frame = self.df[list(self.df.keys())[index]]
            subsequent_frame.loc[-1] = subsequent_frame.columns.tolist()
            subsequent_frame.index = subsequent_frame.index + 1  # shifting index
            subsequent_frame.sort_index(inplace=True) 
            subsequent_frame.columns = col
            list_DF.append(subsequent_frame)

        self.df = pd.concat(list_DF, ignore_index= True)
        self.df = self.df[1:]  


    def readContent(self):
        """
        Extracting Default List to Encryption and Removed List\\
        param: None\\
        output: None
        """
        columns = self.df.columns.tolist()
        self.encapsulationList = self.filter_view(columns, self.defaultEncapsulationList)
        if self.mode == 1:
            self.removeList = self.filter_view(columns, self.defaultRemovedList)
    

    def offset(self, ops):
        """
        Return list difference\\
        param: ops: List\\
        output: List
        """
        return list(set(self.df) - set(ops))


    def filter_view(self, columnList, defaultList):
        """
        Return list of class df column like in default list
        param: columnList: List, defaultList: List
        output: temp: List
        """
        temp = []
        for default in defaultList:
            for file_col in columnList:
                if (default.lower() in file_col.lower()): 
                    if(file_col not in temp and ( file_col not in self.encapsulationList and file_col not in self.removeList)):
                        temp.append(file_col)
        return temp
    
        
    def dropColumns(self):
        """
        Drop class df column given remove list\\
        param: None\\
        output: None
        """
        self.df.drop(self.removeList, axis=1, inplace=True)
    
 
    def encapColumns(self, progress_bar = None): 
        """
        Looping through class df column and encrypt before updating back to class df\\
        param: progress_bar: ProgressBar\\
        output: None
        """ 
        try:
            totalrow = self.df.shape[0]
            totalcol = len(self.encapsulationList)
            total = totalrow * totalcol
            curr = 0
            for column in (self.encapsulationList):
                self.df[[column]] = self.df[[column]].astype(str)
                for index, row in self.df.iterrows():
                    colValue = str(self.df.at[index, column])
                    if len(colValue) > 0:
                        self.df.at[index, column]=  self.cipherObj.encrypt((colValue))
                    if (curr % 100 == 0 and progress_bar is not None):
                        progress_bar.UpdateBar(((curr/ total))* 99)
                    curr = curr+1
        except Exception as ex:
            print(ex)
    
         
    def decapColumns(self, progress_bar = None):
        """
        Looping through class df column and decrypt before updating back to class df\\
        param: progress_bar: ProgressBar\\
        output: None
        """    
        totalrow = self.df.shape[0]
        totalcol = len(self.encapsulationList)
        total = totalrow * totalcol
        curr = 0
        for column in (self.encapsulationList):
            for index, row in self.df.iterrows():
                self.df.at[index, column]=  self.cipherObj.decrypt(self.df.at[index, column])
                if (curr % 100 == 0 and progress_bar is not None):
                    progress_bar.UpdateBar(((curr/ total))* 99)
                curr = curr+1
    
     
    def setAttribute(self, workbook):
        """
        Setting excel file settings and return workbook object\\
        param: workbook : object\\
        output: workbook: object
        """
        if self.mode == 1:
            data = {
                'mode': 'Encryption',
                'value': self.encapsulationList
            }

        else:
            data = ''
        
        workbook.set_properties({
            'keywords': data,
        })
        return workbook
    

    def getAttribute(self):
        """
        Get excel file settings, determine file ops and set mode type\\
        param: None\\
        output: None
        """
        workbook = openpyxl.load_workbook(self.filePath)
        if workbook.properties.keywords != None:
            attribute = (eval(workbook.properties.keywords))
            mode_file = attribute['mode']
            self.defaultEncapsulationList = attribute['value']
            if mode_file == 'Encryption':
                self.mode = 2
        else:
            self.mode = 1

   
    def writeFile(self, filepath):
        """
        Write df to excel\\
        param: None\\
        output: None
        """
        # prepare data
        data = self.df.values.tolist()
        columns = self.df.columns.tolist()
        data.insert(0, columns)
        # load data
        workbook = xlsxwriter.Workbook(filepath)
        worksheet = workbook.add_worksheet()
        for row ,items in enumerate(data):
            for col, item in enumerate(items):
                worksheet.write(row, col, item)
        self.setAttribute(workbook)
        workbook.close()

  
    def writeDefaultList(self):
        """
        Create new predefine data to JSON file\\
        param: None\\
        output: None
        """    
        new_remove = []
        new_encap = []
        [new_remove.append(x) for x in (" ".join(self.removeList)).split() if x not in new_remove]
        [new_encap.append(x) for x in (" ".join(self.encapsulationList)).split() if x not in new_encap]
        settings = {
            'default_remove': new_remove,
            'default_encap': new_encap
        }
        with open('settings.json', 'w') as json_file:
            json.dump(settings, json_file)
            
            
    def readDefaultList(self):
        """
        Read predefine JSON file and update to default list\\
        param: None\\
        output: None
        """    
        if (Path('settings.json').exists()):
            with open('settings.json') as f:
                data = json.load(f)
                self.defaultRemovedList = (data['default_remove'])
                self.defaultEncapsulationList = (data['default_encap'])


    def music(self):
        """
        Play Music\\
        param: None\\
        output: None
        """  
        playsound('../sound/done.mp3')
                
        
    def getFileName(self):
        """
        get suggested file name base on file path\\
        param: None\\
        output: filename_new: String
        """  
        filename = Path(self.filePath).stem
        if self.mode == 1:
            filename_new = '%s(%s)' % (filename, 'Encrypted')
        else:
            if '(Encrypted)' in filename:
                filename_new = filename.replace('Encrypted', 'Decrypted')
            else:
                filename_new = '%s(%s)' % (filename, 'Decrypted')
                                           
        return filename_new
    
    
if __name__ == '__main__':
    pass


    