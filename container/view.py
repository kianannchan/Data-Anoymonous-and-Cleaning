import controller
import interface

# ini class constructor
obj = controller.Model()
interfaceObj = interface.interfaceClass()

# load Interface obj
window = interfaceObj.load()

# update encap/ removal list
def update_list(element, new_val):
    window.FindElement(element).Update(values=new_val)

# header existence check    
def checkForHeader(isFileHeader):
    if (isFileHeader):
        response =interfaceObj.popup_yesno('Headers detected for all sheet. Proceed with this mode?')
        if (response == 'Yes'):
            user_response = 1
        else:
            user_response = 2
    else:
        response =interfaceObj.popup_yesno('No common header found. Proceed with casading mode?')
        if response == 'Yes':
            user_response = 2
        else:
            user_response =1
    if user_response == 1:
        obj.SubsequentHeader()
    else:
        obj.noSubsequentHeader()

# enable process iff item exist in encap/ removel list        
def enableProcessBtn():
    if len(obj.encapsulationList) > 0 or len(obj.removeList) > 0:
        window.FindElement('process').Update(disabled=False)
    else:
        window.FindElement('process').Update(disabled=True)

# update log onto interface        
def updateLog(msg):
    msg = '%s %s\n' % (obj.getDateTimeReadable(), msg)
    obj.log = obj.log + msg
    window.FindElement('-ML-').Update(obj.log)
 
while True:
    # listen for event emitter and its value        
    event, values = window.Read()  
    progress_bar = window.FindElement('progressbar')
    
    # window close
    if event in (None, 'Exit'):
        break
    # browse file
    if event == 'browse_file':
        obj = controller.Model() # reini obj
        filePath = values["browse_file"]

        # file is selected to proceed
        if len(filePath) > 0 :
            # set and read file
            obj.setFilePath(filePath)
            
            # if file is processed, likely xlsx format
            # return mode type if keyword dictionary is found
            # mode 1 = Encryption Mode, mode 2 = Decryption Mode
            # if none return, means file is raw
            if 'xlsx' in filePath:
                obj.getAttribute()

            # check if merging is necessary
            if obj.mode == 1:
                obj.readDefaultList()
                if 'csv' not in filePath:
                    isFileHeader = obj.detectCommonHeader()
                    checkForHeader(isFileHeader)

            else:
                obj.SubsequentHeader()
                
            # Filter encryption and Remove List 
            obj.readContent()

            # enabling buttons and list on interface
            progress_bar.UpdateBar(0)
            not_encap = obj.offset(obj.encapsulationList)
            not_remove = obj.offset(obj.removeList)
            update_list('encap_list_default', not_encap)
            update_list('encap_list_add', obj.encapsulationList)
            update_list('remove_list_default', not_remove)
            update_list('remove_list_add', obj.removeList)
            enableProcessBtn()
            window.FindElement('password').Update(disabled=False)

            # file meant for decryption process
            if (obj.mode == 2):
                obj.setPassword('') # override default_password
                window.FindElement('remove_en').Update(disabled=True)
                window.FindElement('add_en').Update(disabled=True)
                window.FindElement('remove_remove').Update(disabled=True)
                window.FindElement('remove_add').Update(disabled=True)
                window.FindElement('process').Update('Decrypt')

            # file meant for encryption process
            else:
                window.FindElement('remove_en').Update(disabled=False)
                window.FindElement('add_en').Update(disabled=False)
                window.FindElement('remove_remove').Update(disabled=False)
                window.FindElement('remove_add').Update(disabled=False)
                window.FindElement('process').Update('Encrypt')

            # notify user it is done
            obj.music()
            # send notification
            interfaceObj.popup('Data Loaded')
            updateLog('Data Populated')


    # listen for remove encrption <<
    elif event == 'remove_en':
        selection = values['encap_list_add']
        # at least being selected
        if len(selection) > 0:
            # update class list and update interface
            obj.encapsulationList.remove(selection[0])
            not_en = obj.offset(obj.encapsulationList)
            update_list('encap_list_default', not_en)
            update_list('encap_list_add', obj.encapsulationList)
            enableProcessBtn()
            updateLog('Remove [%s] from Encapsulation List' % selection[0])

    # listen for add encryption >>        
    elif event == 'add_en':
        selection = values['encap_list_default']
        # at least being selected
        if len(selection) > 0:
            # cross duplication in removelist
            if selection[0] not in obj.removeList:
                # update class list and update interface
                obj.encapsulationList.append(selection[0])
                not_en = obj.offset(obj.encapsulationList)
                update_list('encap_list_default', not_en)
                update_list('encap_list_add', obj.encapsulationList)
                enableProcessBtn()
                updateLog('Add [%s] to Encapsulation List' % selection[0])
                
            else:
                # notify of duplication
                interfaceObj.popup("Selected item exist in removed list")

    # listen for remove remove <<
    elif event == 'remove_remove':
        selection = values['remove_list_add']
        # at least one selected
        if len(selection) > 0:
            # update class list and update interface
            obj.removeList.remove(selection[0])
            not_re = obj.offset(obj.removeList)
            update_list('remove_list_default', not_re)
            update_list('remove_list_add',obj.removeList)
            enableProcessBtn()
            updateLog('Remove [%s] from removal List' % selection[0])
          
    # listen for remove add >>
    elif event == 'remove_add':
        selection = values['remove_list_default']
        # at least one selected
        if len(selection) > 0:
            # cross duplication in encrption list
            if selection[0] not in obj.encapsulationList:
                # update class list and interface
                obj.removeList.append(selection[0])
                not_re = obj.offset(obj.removeList)
                update_list('remove_list_default', not_re)
                update_list('remove_list_add', obj.removeList)
                enableProcessBtn()
                updateLog('Add [%s] from removal List' % selection[0])

            else:
                # notify of duplication
                interfaceObj.popup("Selected item exist in encapsulation list")

    # listen for process button
    elif event == 'process':
        start = obj.getTimeEpochTime()
        # disabled button during processing
        window.FindElement('process').Update(disabled=True)
        window.FindElement('remove_en').Update(disabled=True)
        window.FindElement('add_en').Update(disabled=True)
        window.FindElement('remove_remove').Update(disabled=True)
        window.FindElement('remove_add').Update(disabled=True)
        
        # set password if fields is not blank
        if len(values['password'])> 0:
            obj.setPassword(values['password'])

        # encryption mode
        if obj.mode == 1:
            updateLog('Start encryption process')
            updateLog('Start drop column')
            obj.dropColumns()
            updateLog('Finish drop column')
            updateLog('Start encrypt column')
            obj.encapColumns(progress_bar)
            updateLog('Finish encrypt column')

            
        # decryption mode
        else:
            updateLog('Start decryption process')
            updateLog('Start decrypt column')
            obj.decapColumns(progress_bar)
            updateLog('Finish decrypt column')
        
        # open file saveas dialog
        newPath = interfaceObj.saveFile(obj.getFileName())
        
        # call write method
        updateLog('Start writing file')
        obj.writeFile(newPath)
        updateLog('Finish writing file')
        progress_bar.UpdateBar(100)
        obj.music()
        interfaceObj.tray('Done processing! Time taken: %0.2fs ' %  (obj.getTimeDiff(start)))
        
        # update keywords
        if obj.mode ==1:
            response = interfaceObj.popup_yesno('Update Keywords?')
            if (response == 'Yes'):
                obj.writeDefaultList()
                updateLog('Keywords updated')



window.Close()

