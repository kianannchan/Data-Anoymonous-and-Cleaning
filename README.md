# Data Anoymonous and Cleaning (DAAC)
*Data Anoymonous and Cleaning (DAAC) is a tool developed in python 3.7.8. Objective of the tool allows the user to removed unecessary 
columns or/and hide sensitive data within the application itself. After that, the processed file could be transferred to other system safely
without revealing sensitive data. To recover the anoymonous data, the file could be feed into the same tool by providing the original secret key*


#
### Features
- [x] Upload targetted source file
- [x] Predefined encrypted or removed keywords and load into the interface
- [x] Progressive progress bar
- [x] Encrypted columns values are reverable with the correct passphrase provided
- [x] Dynamically predefine list by files
- [x] Support Multiple tab and parse them as singular dataframe
- [x] Reflect the current state of the operations in the logging section

#
### Encryption and Decryption
*Uses Fernet Key Cryptography - implementation of symmetric (also known as “secret key”) authenticated cryptography. The key generation is defined below:* 

```
def setKey(self, password):
    password = password.encode() 
    salt = b'SQLShack_' 
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=2000,
        backend=default_backend()
        )
    self.key = base64.urlsafe_b64encode(kdf.derive(password)) 
```

#### System Properties
*Below table describes the system specifications use for the test. A better system (with higher memory and computation speed) would impact the results* 

| Parameters  |  Values |
| ------------- | ------------- |
| Processor  | Intel(R) Core(TM) i5-7400 CPU @ 3.00GHz, 3000 Mhz, 4 Core(s), 4 Logical Processor(s) |
| Physical Memory  | 8 Gb  |

#### Test Result
*Below table describes the encryption/ decryption time taken given variance in number of encyption columns. 100,000 rows record with 13
columns is used for the test.*


| No. of Enc Columns  | No. of Enc Cells | Time Taken (s) |
| ------------- | ------------- | ------------- |
| 2  | 200,000  | 71.71 |
| 8  | 800,000  | 219.18 |



#
### Software Model

```mermaid
graph TD;
  view.py!View-->interface.py!View;
  view.py!View-->controller.py!Model/Controller;
  controller.py!Model/Controller-->Cipher.py
```

#
### Demonstration
[![Data-Anoymonous-and-Cleaning-DAAC.Encryptyion-Mode.gif](https://s7.gifyu.com/images/Data-Anoymonous-and-Cleaning-DAAC.Encryptyion-Mode.gif)](https://gifyu.com/image/WXMc)



#
### Usage
```
pip install -r requirements.txt
cd container
python view.py
```

#
### Development
- [ ] Integrated parallelism pandas mechanism. i.e. Dask

