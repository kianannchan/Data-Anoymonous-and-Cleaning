import unittest
import sys
#sys.path.append('../container')
from controller import Model  


class SimpleTest(unittest.TestCase): 
    
    def setUp(self):
        self.obj = Model()
        baseLink = '../Files/'
        self.xls_common_header_true = baseLink+'xls_common_header_true.xls'
        self.xls_common_header_false = baseLink+'xls_common_header_false.xls'
        self.xls_header_no_common_shape = baseLink+'xls_header_no_common_shape.xls'
        self.csv_Sales_Records = baseLink+'100 Sales Records.csv'

        
    # Test for header existence
    def testHeaderExistenceTrue(self):
        self.obj.setFilePath(self.xls_common_header_true)
        response = self.obj.detectCommonHeader()
        self.assertEqual(response, True)
        
    # Test for header existence
    def testHeaderExistenceFalse(self):
        self.obj.setFilePath(self.xls_common_header_false)
        response = self.obj.detectCommonHeader()
        self.assertEqual(response, False)
        
    # Test header existence merge size
    def testHeaderExistenceTrueShape(self):
        self.obj.setFilePath(self.xls_common_header_true)
        self.obj.SubsequentHeader()
        self.assertEqual(self.obj.df.shape,(200,14))
        
    # Test unique header existence merge size
    def testUniqueHeaderExistenceTrueShape(self):
        self.obj.setFilePath(self.xls_header_no_common_shape)
        self.obj.SubsequentHeader()
        firstHalf =  (list(self.obj.df['new col'])[:100])
        response = (len(set(firstHalf).difference(set(''))))
        self.assertEqual(response,1)
        
    # Test header existence merge size
    def testHeaderExistenceFalseShape(self):
        self.obj.setFilePath(self.xls_common_header_false)
        self.obj.noSubsequentHeader()
        self.assertEqual(self.obj.df.shape,(200,14))
    
    # Test for encapsulation list
    def testEncapsulationList(self):
        self.obj.setFilePath(self.csv_Sales_Records)
        self.obj.defaultEncapsulationList = ['Ship Date', 'Total Revenue']
        self.obj.readContent()  
        self.assertEqual(self.obj.defaultEncapsulationList, self.obj.encapsulationList) 
        
    # Test Removal list
    def testRemovalList(self): 
        self.obj.setFilePath(self.csv_Sales_Records) 
        self.obj.defaultRemovedList = ['Ship Date', 'Total Revenue'] 
        self.obj.readContent()      
        self.assertEqual(self.obj.defaultRemovedList, self.obj.removeList) 
        
    # Test Removal list
    def testRemoval(self): 
        self.obj.setFilePath(self.csv_Sales_Records) 
        self.obj.defaultRemovedList = ['Ship Date'] 
        self.obj.readContent()  
        self.obj.dropColumns()
        afterRemoval = self.obj.df.columns.tolist()   
        self.assertNotIn('Ship Date', afterRemoval)
        
    # Test encap/ decap correct passphrase provided 
    def testEncapAndDecapsulationCorrect(self): 
        self.obj.setFilePath(self.csv_Sales_Records) 
        self.obj.defaultEncapsulationList = ['Ship Date'] 
        self.obj.readContent()
        before_encapsulation = self.obj.df['Ship Date'].tolist()
        self.obj.setPassword('chickenMacNugget')
        self.obj.encapColumns()
        self.obj.setPassword('chickenMacNugget')
        self.obj.decapColumns()
        after_decapsulation = self.obj.df['Ship Date'].tolist()
        self.assertEqual(before_encapsulation,after_decapsulation)
        
    # Test encap/ decap wrong passphrase provided
    def testEncapAndDecapsulationIncorrect(self): 
        self.obj.setFilePath('../files/100 Sales Records.csv') 
        self.obj.defaultEncapsulationList = ['Ship Date'] 
        self.obj.readContent()
        before_encapsulation = self.obj.df['Ship Date'].tolist()
        self.obj.setPassword('chickenMacNugget')
        self.obj.encapColumns()
        self.obj.setPassword('chickenMacNuggetsss')
        self.obj.decapColumns()
        after_decapsulation = self.obj.df['Ship Date'].tolist()
        self.assertNotEqual(before_encapsulation,after_decapsulation)
  
if __name__ == '__main__': 
    unittest.main() 
    

    